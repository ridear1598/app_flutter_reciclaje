import 'package:flutter/material.dart';
import 'package:flutter_reciclaje/src/pages/administrador/orders/list/restaurant_orders_list_page.dart';
import 'package:flutter_reciclaje/src/pages/client/adress/create/client_adress_create_page.dart';
import 'package:flutter_reciclaje/src/pages/client/adress/list/client_adress_list_page.dart';
import 'package:flutter_reciclaje/src/pages/client/adress/map/client_adress_map_page.dart';
import 'package:flutter_reciclaje/src/pages/client/entregas/create/entregas_create_page.dart';
import 'package:flutter_reciclaje/src/pages/client/entregas/list/client_orders_list_page.dart';
import 'package:flutter_reciclaje/src/pages/client/entregas/map/client_orders_map_page.dart';
import 'package:flutter_reciclaje/src/pages/client/products/list/clients_products_list_page.dart';
import 'package:flutter_reciclaje/src/pages/client/update/client_update_page.dart';
import 'package:flutter_reciclaje/src/pages/login/login_page.dart';
import 'package:flutter_reciclaje/src/pages/reciclador/orders/list/reciclador_orders_list_page.dart';
import 'package:flutter_reciclaje/src/pages/register/register_page.dart';
import 'package:flutter_reciclaje/src/pages/roles/roles_page.dart';
import 'package:flutter_reciclaje/src/utils/my_colors.dart';

import 'src/pages/reciclador/orders/map/entregas_orders_map_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Delibery app flutter',
        initialRoute: 'login',
        //theme: ThemeData(primaryColor: Colors.deepOrange),
        theme: ThemeData(
            // fontFamily: 'NimbusSans',
            primaryColor: MyColors.primaryColor),
        routes: {
          'login': (BuildContext context) => LoginPage(),
          'register': (BuildContext context) => RegisterPage(),
          'roles': (BuildContext context) => RolesPage(),
          'client/update': (BuildContext context) => ClienteUpdatePage(),
          'client/products/list': (BuildContext context) =>
              ClientOrdersListPage(),
          'client/entregas/list': (BuildContext context) =>
              ClientOrdersListPage(),
          'client/map/list': (BuildContext context) => ClientOrdenesMapPage(),
          'restaurant/orders/list': (BuildContext context) =>
              RestaurantOrdersListPage(),
          'delivery/orders/list': (BuildContext context) =>
              RecicladorOrdersListPage(),
          'client/adress/list': (BuildContext context) =>
              ClientAdressListPage(),
          'client/adress/create': (BuildContext context) =>
              ClientAdressCreatePage(),
          'client/adress/map': (BuildContext context) => ClientAdressMapPage(),
          'entrega/reciclaje/create': (BuildContext context) => EntregasPage(),
          'reciclador/entregas/map': (BuildContext context) =>
              EntregasOrdenesMapPage()
        });
  }
}
