import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_reciclaje/src/api/enviroment.dart';
import 'package:flutter_reciclaje/src/models/address.dart';
import 'package:flutter_reciclaje/src/models/entregas.dart';
import 'package:flutter_reciclaje/src/models/response_api.dart';
import 'package:http/http.dart' as http;

class EntregasProvider {
  String _url = Environment.API_DELIVERY;
  String _api = '/api/entregas';
  BuildContext context;

  Future init(BuildContext context) async {
    this.context = context;
  }

  Future<ReesponseApi> createEntrega(Entregas entregas) async {
    try {
      Uri url = Uri.http(_url, '$_api/create');
      String bodyParams = json.encode(entregas);
      Map<String, String> headers = {'Content-type': 'application/json'};

      final res = await http.post(url, headers: headers, body: bodyParams);
      final data = json.decode(res.body);
      print('La data es $data');
      ReesponseApi responseApi = ReesponseApi.fromJson(data);
      return responseApi;
    } catch (e) {
      print('error $e');
      return null;
    }
  }

//Buscamos un usuario por id
  Future<List<Entregas>> getByStatus(String status) async {
    try {
      Uri url = Uri.http(_url, '$_api/findByStatus/$status');
      Map<String, String> headers = {'Content-type': 'application/json'};

      final res = await http.get(url, headers: headers);
      final data = json.decode(res.body);
      print('Lista de $data');

      Entregas entrega = Entregas.fromJsonList(data);
      return entrega.toList;
    } catch (e) {
      print('error $e');
      return [];
    }
  }

  Future<List<Entregas>> getByRecicladorStatus(
      String idDelivery, String status) async {
    try {
      Uri url =
          Uri.http(_url, '$_api/findByRecicladorAndStatus/$idDelivery/$status');
      Map<String, String> headers = {'Content-type': 'application/json'};

      final res = await http.get(url, headers: headers);
      final data = json.decode(res.body);
      print('Lista de $data');

      Entregas entrega = Entregas.fromJsonList(data);
      return entrega.toList;
    } catch (e) {
      print('error $e');
      return [];
    }
  }

//Buscamos un usuario por id
  Future<List<Address>> getByUser(String id_user) async {
    try {
      Uri url = Uri.http(_url, '$_api/findByUser/$id_user');
      Map<String, String> headers = {'Content-type': 'application/json'};
      final res = await http.get(url, headers: headers);
      final data = json.decode(res.body);
      print('Lista de $data');

      Address address = Address.fromJsonList(data);
      return address.toList;
    } catch (e) {
      print('error $e');
      return null;
    }
  }

  Future<ReesponseApi> updateToDispach(Entregas entregas) async {
    try {
      Uri url = Uri.http(_url, '$_api/updateToEntrega');
      String bodyParams = json.encode(entregas);
      Map<String, String> headers = {'Content-type': 'application/json'};

      final res = await http.put(url, headers: headers, body: bodyParams);
      final data = json.decode(res.body);
      print('La data es $data');
      ReesponseApi responseApi = ReesponseApi.fromJson(data);
      return responseApi;
    } catch (e) {
      print('error $e');
      return null;
    }
  }

  Future<ReesponseApi> updateToEnCamino(Entregas entregas) async {
    try {
      Uri url = Uri.http(_url, '$_api/updateToEncamino');
      String bodyParams = json.encode(entregas);
      Map<String, String> headers = {'Content-type': 'application/json'};

      final res = await http.put(url, headers: headers, body: bodyParams);
      final data = json.decode(res.body);
      print('La data es $data');
      ReesponseApi responseApi = ReesponseApi.fromJson(data);
      return responseApi;
    } catch (e) {
      print('error $e');
      return null;
    }
  }

  Future<ReesponseApi> updateToEnCaminoReciclador(Entregas entregas) async {
    try {
      Uri url = Uri.http(_url, '$_api/updateToEncaminoReciclador');
      String bodyParams = json.encode(entregas);
      Map<String, String> headers = {'Content-type': 'application/json'};

      final res = await http.put(url, headers: headers, body: bodyParams);
      final data = json.decode(res.body);
      print('La data es $data');
      ReesponseApi responseApi = ReesponseApi.fromJson(data);
      return responseApi;
    } catch (e) {
      print('error $e');
      return null;
    }
  }

  Future<List<Entregas>> getByContribuyenteStatus(
      String idControbuyente, String status) async {
    try {
      Uri url = Uri.http(
          _url, '$_api/findByContribuyenteAndStatus/$idControbuyente/$status');
      Map<String, String> headers = {'Content-type': 'application/json'};

      final res = await http.get(url, headers: headers);
      final data = json.decode(res.body);
      print('Lista de $data');

      Entregas entrega = Entregas.fromJsonList(data);
      return entrega.toList;
    } catch (e) {
      print('error $e');
      return [];
    }
  }
}
