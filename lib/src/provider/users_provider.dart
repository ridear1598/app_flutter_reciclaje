import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_reciclaje/src/api/enviroment.dart';
import 'package:flutter_reciclaje/src/models/response_api.dart';
import 'package:flutter_reciclaje/src/models/user.dart';
import 'package:http/http.dart' as http;

class UsersProvider {
  String _url = Environment.API_DELIVERY;
  String _api = '/api/users';
  BuildContext context;

  Future init(BuildContext context) async {
    this.context = context;
  }

  Future<ReesponseApi> create(User user) async {
    try {
      Uri url = Uri.http(_url, '$_api/register');
      String bodyParams = json.encode(user);
      Map<String, String> headers = {'Content-type': 'application/json'};

      final res = await http.put(url, headers: headers, body: bodyParams);
      final data = json.decode(res.body);
      print('La data es $data');
      ReesponseApi responseApi = ReesponseApi.fromJson(data);
      return responseApi;
    } catch (e) {
      print('error $e');
      return null;
    }
  }

//Buscamos un usuario por id
  Future<User> getById(String id) async {
    try {
      Uri url = Uri.http(_url, '$_api/findById/$id');
      Map<String, String> headers = {'Content-type': 'application/json'};
      final res = await http.get(url, headers: headers);
      final data = json.decode(res.body);
      User user = User.fromJson(data);
      return user;
    } catch (e) {
      print('error $e');
      return null;
    }
  }

  Future<List<User>> getReciladorMen() async {
    try {
      Uri url = Uri.http(_url, '$_api/findReciclador');
      Map<String, String> headers = {'Content-type': 'application/json'};
      final res = await http.get(url, headers: headers);
      final data = json.decode(res.body);
      User user = User.fromJsonList(data);
      return user.toList;
    } catch (e) {
      print('error $e');
      return null;
    }
  }

  Future<ReesponseApi> update(User user) async {
    try {
      Uri url = Uri.http(_url, '$_api/update');
      String bodyParams = json.encode(user);
      Map<String, String> headers = {'Content-type': 'application/json'};

      final res = await http.put(url, headers: headers, body: bodyParams);
      final data = json.decode(res.body);
      ReesponseApi responseApi = ReesponseApi.fromJson(data);
      return responseApi;
    } catch (e) {
      print('error $e');
      return null;
    }
  }

  Future<ReesponseApi> login(String dni, String password) async {
    try {
      Uri url = Uri.http(_url, '$_api/login');
      String bodyParams = json.encode({'dni': dni, 'password': password});
      Map<String, String> headers = {'Content-type': 'application/json'};

      final res = await http.post(url, headers: headers, body: bodyParams);
      final data = json.decode(res.body);
      ReesponseApi responseApi = ReesponseApi.fromJson(data);
      return responseApi;
    } catch (e) {
      print('error $e');
      return null;
    }
  }
}
