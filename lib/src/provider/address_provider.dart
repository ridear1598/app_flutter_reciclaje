import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_reciclaje/src/api/enviroment.dart';
import 'package:flutter_reciclaje/src/models/address.dart';
import 'package:flutter_reciclaje/src/models/response_api.dart';
import 'package:http/http.dart' as http;

class AddressProvider {
  String _url = Environment.API_DELIVERY;
  String _api = '/api/adress';
  BuildContext context;

  Future init(BuildContext context) async {
    this.context = context;
  }

  Future<ReesponseApi> create(Address address) async {
    try {
      Uri url = Uri.http(_url, '$_api/create');
      String bodyParams = json.encode(address);
      Map<String, String> headers = {'Content-type': 'application/json'};

      final res = await http.post(url, headers: headers, body: bodyParams);
      final data = json.decode(res.body);
      print('La data es $data');
      ReesponseApi responseApi = ReesponseApi.fromJson(data);
      return responseApi;
    } catch (e) {
      print('error $e');
      return null;
    }
  }

//Buscamos un usuario por id
  Future<List<Address>> getByUser(String id_user) async {
    try {
      Uri url = Uri.http(_url, '$_api/findByUser/$id_user');
      Map<String, String> headers = {'Content-type': 'application/json'};
      final res = await http.get(url, headers: headers);
      final data = json.decode(res.body);
      print('Lista de $data');

      Address address = Address.fromJsonList(data);
      return address.toList;
    } catch (e) {
      print('error $e');
      return null;
    }
  }
}
