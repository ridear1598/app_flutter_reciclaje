import 'package:flutter/material.dart';
import 'package:flutter_reciclaje/src/models/response_api.dart';
import 'package:flutter_reciclaje/src/models/user.dart';
import 'package:flutter_reciclaje/src/provider/users_provider.dart';
import 'package:flutter_reciclaje/src/utils/my-snackbar.dart';

class RegisterController {
  BuildContext context;
  TextEditingController emailController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController confirmPasswordController = new TextEditingController();
  TextEditingController dniController = new TextEditingController();

  UsersProvider usersProvider = new UsersProvider();

  //Inicializar las variables
  Future init(BuildContext context) async {
    this.context = context;
    await usersProvider.init(context);
  }

  void register() async {
    // String email = emailController.text.trim();
    // String nombre = nameController.text;
    // String apellidos = lastNameController.text;
    // String telefono = phoneController.text.trim();
    String password = passwordController.text.trim();
    String confirmPassword = confirmPasswordController.text.trim();
    String dni = dniController.text.trim();

    if (dni.isEmpty ||
        // nombre.isEmpty ||
        // apellidos.isEmpty ||
        // telefono.isEmpty ||
        password.isEmpty ||
        confirmPassword.isEmpty) {
      MySnackbar.show(context, 'Ingresar todos los campos');
      return;
    }
    if (confirmPassword != password) {
      MySnackbar.show(context, 'Las contraseñas no coinsiden');
      return;
    }

    if (password.length < 6) {
      MySnackbar.show(context, 'Las contraseña debe tener mas de 6 caracteres');
      return;
    }

    User user = new User(
        // email: email,
        // name: nombre,
        dni: dni,
        // phone: telefono,
        password: password);
    // lastname: apellidos);

    ReesponseApi responseApi = await usersProvider.create(user);
    MySnackbar.show(context, responseApi.msg);
    print('RESPUESTA ${responseApi.toJson()}');

    if (responseApi.success) {
      Future.delayed(Duration(seconds: 2), () {
        Navigator.pushReplacementNamed(context, 'login');
      });
    }
  }

  void irLogin() {
    Navigator.pushReplacementNamed(context, 'login');
  }
}
