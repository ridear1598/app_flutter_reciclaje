import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_reciclaje/src/pages/register/register_controller.dart';
import 'package:flutter_reciclaje/src/utils/my_colors.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  RegisterController _con = new RegisterController();

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      _con.init(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: double.infinity,
      child: Stack(
        children: [
          Positioned(top: -80, left: -100, child: _circleLogin()),
          Positioned(child: _textregister(), top: 70, left: 30),
          Positioned(child: _iconBack(), top: 57, left: -5),
          Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 100),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  _imageUser(),
                  SizedBox(height: 30),
                  // _textFieldEmail(),
                  // _textFielName(),
                  // _textFielApellido(),
                  // _textFielPhone(),
                  _textFielDNI(),
                  _textFielPassword(),
                  _textFielConfirmPassword(),
                  _buttonRegister()
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }

  Widget _circleLogin() {
    return Container(
      width: 240,
      height: 230,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          color: MyColors.primaryColor),
    );
  }

  Widget _imageUser() {
    return Container(
      // width: double.infinity,
      // margin: EdgeInsets.only(top: 100),
      child: CircleAvatar(
        backgroundImage: AssetImage('assets/img/user_profile_2.png'),
        radius: 80,
        backgroundColor: Colors.grey[200],
      ),
    );
  }

  Widget _textregister() {
    return Text('Registro',
        style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 22,
            fontFamily: 'NimbusSans'));
  }

  Widget _iconBack() {
    return IconButton(
      onPressed: _con.irLogin,
      icon: Icon(Icons.arrow_back_ios, color: Colors.white),
    );
  }

  Widget _textFieldEmail() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.primaryOpacityColor,
            borderRadius: BorderRadius.circular(30)),
        child: TextField(
          keyboardType: TextInputType.emailAddress,
          controller: _con.emailController,
          decoration: InputDecoration(
              hintText: 'Email',
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(15),
              hintStyle: TextStyle(color: MyColors.primaryColorDark),
              prefixIcon: Icon(Icons.email, color: MyColors.primaryColor)),
        ));
  }

  Widget _textFielName() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.primaryOpacityColor,
            borderRadius: BorderRadius.circular(30)),
        child: TextField(
          controller: _con.nameController,
          decoration: InputDecoration(
              hintText: 'Nombre',
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(15),
              hintStyle: TextStyle(color: MyColors.primaryColorDark),
              prefixIcon: Icon(Icons.person, color: MyColors.primaryColor)),
        ));
  }

  Widget _textFielApellido() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.primaryOpacityColor,
            borderRadius: BorderRadius.circular(30)),
        child: TextField(
          controller: _con.lastNameController,
          decoration: InputDecoration(
              hintText: 'Apellido',
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(15),
              hintStyle: TextStyle(color: MyColors.primaryColorDark),
              prefixIcon:
                  Icon(Icons.person_outline, color: MyColors.primaryColor)),
        ));
  }

  Widget _textFielPhone() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.primaryOpacityColor,
            borderRadius: BorderRadius.circular(30)),
        child: TextField(
          keyboardType: TextInputType.phone,
          controller: _con.phoneController,
          decoration: InputDecoration(
              hintText: 'Telefono',
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(15),
              hintStyle: TextStyle(color: MyColors.primaryColorDark),
              prefixIcon: Icon(Icons.phone, color: MyColors.primaryColor)),
        ));
  }

  Widget _textFielDNI() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.primaryOpacityColor,
            borderRadius: BorderRadius.circular(30)),
        child: TextField(
          keyboardType: TextInputType.phone,
          controller: _con.dniController,
          decoration: InputDecoration(
              hintText: 'DNI',
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(15),
              hintStyle: TextStyle(color: MyColors.primaryColorDark),
              prefixIcon:
                  Icon(Icons.card_membership, color: MyColors.primaryColor)),
        ));
  }

  Widget _textFielPassword() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.primaryOpacityColor,
            borderRadius: BorderRadius.circular(30)),
        child: TextField(
          obscureText: true,
          controller: _con.passwordController,
          decoration: InputDecoration(
              hintText: 'Contraseña',
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(15),
              hintStyle: TextStyle(color: MyColors.primaryColorDark),
              prefixIcon: Icon(Icons.lock, color: MyColors.primaryColor)),
        ));
  }

  Widget _textFielConfirmPassword() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.primaryOpacityColor,
            borderRadius: BorderRadius.circular(30)),
        child: TextField(
          obscureText: true,
          controller: _con.confirmPasswordController,
          decoration: InputDecoration(
              hintText: 'Confirmar contraseña',
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(15),
              hintStyle: TextStyle(color: MyColors.primaryColorDark),
              prefixIcon:
                  Icon(Icons.lock_outline, color: MyColors.primaryColor)),
        ));
  }

  Widget _buttonRegister() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 50, vertical: 30),
      width: double.infinity, //Abarca toda la pantalla
      child: ElevatedButton(
          onPressed: _con.register,
          child: Text('Registrarse  '),
          style: ElevatedButton.styleFrom(
              primary: MyColors.primaryColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),
              padding: EdgeInsets.symmetric(vertical: 15))),
    );
  }
}
