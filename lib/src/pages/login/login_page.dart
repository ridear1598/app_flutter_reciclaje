import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_reciclaje/src/pages/login/login_controller.dart';
import 'package:flutter_reciclaje/src/utils/my_colors.dart';
import 'package:lottie/lottie.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LoginController _con = new LoginController();
  @override
  void initState() {
    //este metodo se ejecutara primero
    super.initState();
    print('INIT STATE');

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      //Se ejecuta despues que la los widgtes de la pantalla se han dibujado
      print('SCHEDULER BINDING');
      _con.init(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    print('METODO BUILD');
    return Scaffold(
        body: Container(
      width: double.infinity, //Abarca toda la pantalla
      child: Stack(
        children: [
          Positioned(top: -80, left: -100, child: _circleLogin()),
          Positioned(child: _textLogin(), top: 60, left: 25),
          SingleChildScrollView(
            child: Column(
              children: [
                // _imageBanner(),
                _lottieAnimation(),
                _textFieldDNI(),
                _textFieldPassword(),
                _buttonLogin(),
                _textCrearCuenta(),
              ],
            ),
          ),
        ],
      ),
    ));
  }

  Widget _textLogin() {
    return Text('Login',
        style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 22,
            fontFamily: 'NimbusSans'));
  }

  Widget _buttonLogin() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 50, vertical: 30),
      width: double.infinity, //Abarca toda la pantalla
      child: ElevatedButton(
          onPressed: _con.login,
          child: Text('Ingresar'),
          style: ElevatedButton.styleFrom(
              primary: MyColors.primaryColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),
              padding: EdgeInsets.symmetric(vertical: 15))),
    );
  }

  Widget _textFieldDNI() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.primaryOpacityColor,
            borderRadius: BorderRadius.circular(30)),
        child: TextField(
          controller: _con.dniController,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
              hintText: 'DNI',
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(15),
              hintStyle: TextStyle(color: MyColors.primaryColorDark),
              prefixIcon:
                  Icon(Icons.card_membership, color: MyColors.primaryColor)),
        ));
  }

  Widget _textFieldPassword() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.primaryOpacityColor,
            borderRadius: BorderRadius.circular(30)),
        child: TextField(
          controller: _con.passwordController,
          obscureText: true,
          decoration: InputDecoration(
              hintText: 'Password',
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(15),
              hintStyle: TextStyle(color: MyColors.primaryColorDark),
              prefixIcon: Icon(Icons.lock, color: MyColors.primaryColor)),
        ));
  }

  /* Widget _imageBanner() {
    return Container(
      margin: EdgeInsets.only(
          top: 100, bottom: MediaQuery.of(context).size.height * 0.10),
      child: Image.asset('assets/img/delivery.png', width: 200, height: 200),
    );
  }
*/

  Widget _circleLogin() {
    return Container(
      width: 240,
      height: 230,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          color: MyColors.primaryColor),
    );
  }

  Widget _lottieAnimation() {
    return Container(
      margin: EdgeInsets.only(
          top: 160, bottom: MediaQuery.of(context).size.height * 0.10),
      child: Lottie.asset('assets/json/reciclaje.json',
          width: 350, height: 200, fit: BoxFit.fill),
    );
  }

  Widget _textCrearCuenta() {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      Text(
        '¿No tienes cuenta?',
        style: TextStyle(color: MyColors.primaryColor),
      ),
      SizedBox(width: 7),
      GestureDetector(
        onTap: _con.goToregisterPage,
        child: Text('Registrate',
            style: TextStyle(
                fontWeight: FontWeight.bold, color: MyColors.primaryColor)),
      )
    ]);
  }
}
