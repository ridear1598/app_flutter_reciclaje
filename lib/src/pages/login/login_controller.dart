import 'package:flutter/material.dart';
import 'package:flutter_reciclaje/src/models/response_api.dart';
import 'package:flutter_reciclaje/src/models/user.dart';
import 'package:flutter_reciclaje/src/provider/users_provider.dart';
import 'package:flutter_reciclaje/src/utils/my-snackbar.dart';
import 'package:flutter_reciclaje/src/utils/shared_pref.dart';

class LoginController {
  BuildContext context;
  TextEditingController dniController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  UsersProvider usersProvider = new UsersProvider();
  SharedPref _sharedPref = new SharedPref();

  Future init(BuildContext context) async {
    this.context = context;
    await usersProvider.init(context);

    User user = User.fromJson(await _sharedPref.read('user') ?? {});
    //print('User es ${user.toJson()}');
    //? singnifica if(user!=null)
    if (user?.sessionToken != null) {
      //Navigator.pushNamedAndRemoveUntil(context, 'clint/products/list', (route) => false);
      if (user.roles.length > 1) {
        Navigator.pushNamedAndRemoveUntil(context, 'roles', (route) => false);
      } else {
        Navigator.pushNamedAndRemoveUntil(
            context, user.roles[0].route, (route) => false);
      }
    }
  }

  void goToregisterPage() {
    Navigator.pushNamed(context, 'register');
  }

  void login() async {
    String dni = dniController.text.trim();
    String password = passwordController.text.trim();
    ReesponseApi responseApi = await usersProvider.login(dni, password);

    print('respuesta 2 ${responseApi.toJson()}');

    if (responseApi.success) {
      //Resivimos un json y lo trasformamos a un objeto user
      User user = User.fromJson(responseApi.data);
      _sharedPref.save('user', user.toJson());

      print('USER LOGEADO: ${user.toJson()}');
      if (user.roles.length > 1) {
        Navigator.pushNamedAndRemoveUntil(context, 'roles', (route) => false);
      } else {
        //Nos lleva a la siguiente pantalla y elimina el historial de pantallas anterior
        // Navigator.pushNamedAndRemoveUntil(context, 'clint/products/list', (route) => false);
        Navigator.pushNamedAndRemoveUntil(
            context, user.roles[0].route, (route) => false);
      }
    } else {
      MySnackbar.show(context, responseApi.msg);
    }
  }
}
