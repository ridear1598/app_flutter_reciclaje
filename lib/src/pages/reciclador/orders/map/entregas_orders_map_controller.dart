import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_reciclaje/src/api/enviroment.dart';
import 'package:flutter_reciclaje/src/models/entregas.dart';
import 'package:flutter_reciclaje/src/models/response_api.dart';
import 'package:flutter_reciclaje/src/provider/entregas_provider.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:location/location.dart' as location;
import 'package:url_launcher/url_launcher.dart';

class EntregasOrdenesMapController {
  BuildContext context;
  Function refresh;
  Position _position;
  StreamSubscription _positionSteam;
  String adreesName;
  LatLng adreesLatLng;
  EntregasProvider _entregasProvider = new EntregasProvider();

  double _distaceBetween;

  CameraPosition initialPosition =
      CameraPosition(target: LatLng(-8.0880456, -79.0287179), zoom: 14);

  Completer<GoogleMapController> _mapController = Completer();
  BitmapDescriptor recicladorMarquer;
  BitmapDescriptor toMarker;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Entregas entrega;
  Set<Polyline> polylines = {};
  List<LatLng> points = [];

  Future init(BuildContext context, Function refresh) async {
    this.context = context;
    this.refresh = refresh;

    entrega = Entregas.fromJson(
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>);

    recicladorMarquer =
        await createMarkerFromAsstes('assets/img/delivery2.png');
    toMarker = await createMarkerFromAsstes('assets/img/home.png');
    _entregasProvider.init(context);
    checkPGS();
  }

//Trazar ruta en el mapa
  Future<void> setPolylines(LatLng from, LatLng to) async {
    PointLatLng pointFrom = PointLatLng(from.latitude, from.longitude);
    PointLatLng pointTo = PointLatLng(to.latitude, to.longitude);
    PolylineResult result = await PolylinePoints().getRouteBetweenCoordinates(
        Environment.API_KEY_MAPS, pointFrom, pointTo);

    for (PointLatLng point in result.points) {
      points.add(LatLng(point.latitude, point.longitude));
    }
    Polyline polyline = Polyline(
        polylineId: PolylineId('poly'),
        color: Colors.red,
        points: points,
        width: 7);

    polylines.add(polyline);
    refresh();
  }

  void addMarker(String markerId, double lat, double lng, String title,
      String content, BitmapDescriptor iconMarker) {
    MarkerId id = MarkerId(markerId);

    Marker marker = Marker(
        markerId: id,
        icon: iconMarker,
        position: LatLng(lat, lng),
        infoWindow: InfoWindow(title: title, snippet: content));

    markers[id] = marker;

    refresh();
  }

  void selectRefPoint() {
    Map<String, dynamic> data = {
      'adress': adreesName,
      'lat': adreesLatLng.latitude,
      'lng': adreesLatLng.longitude
    };
    Navigator.pop(context, data);
  }

  Future<BitmapDescriptor> createMarkerFromAsstes(String path) async {
    ImageConfiguration configuration = ImageConfiguration();
    BitmapDescriptor descriptor =
        await BitmapDescriptor.fromAssetImage(configuration, path);

    return descriptor;
  }

  Future<Null> setLocationDraggableInfo() async {
    if (initialPosition != null) {
      double lat = initialPosition.target.latitude;
      double lng = initialPosition.target.longitude;

      List<Placemark> adrees = await placemarkFromCoordinates(lat, lng);

      if (adrees != null) {
        if (adrees.length > 0) {
          String direction = adrees[0].thoroughfare;
          String calle = adrees[0].subThoroughfare;
          String city = adrees[0].locality;
          String departament = adrees[0].administrativeArea;
          // String pais = adrees[0].country;
          adreesName = '$direction #$calle, $city, $departament';
          adreesLatLng = new LatLng(lat, lng);
          refresh();
        }
      }
    }
  }

  void onMapCreated(GoogleMapController controller) {
    // controller.setMapStyle(
    //     '[{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]');
    _mapController.complete(controller);
  }

  void dispose() {
    _positionSteam?.cancel();
  }

  void updateLocation() async {
    try {
      await _determinePosition(); //Obtener la posicion actual y solicitar los permisos
      _position =
          await Geolocator.getLastKnownPosition(); //Latitud y longitud actual
      animateCameraToPosicion(_position.latitude, _position.longitude);
      addMarker('reciclador', _position.latitude, _position.longitude,
          'Tu posicion', ' ', recicladorMarquer);

      addMarker('home', entrega.address.lat, entrega.address.lng, 'Tu posicion',
          ' ', toMarker);

      LatLng from = new LatLng(_position.latitude, _position.longitude);
      LatLng to = new LatLng(entrega.address.lat, entrega.address.lng);
      setPolylines(from, to);

      _positionSteam = Geolocator.getPositionStream(
              desiredAccuracy: LocationAccuracy.best, distanceFilter: 1)
          .listen((Position position) {
        _position = position;
        addMarker('reciclador', _position.latitude, _position.longitude,
            'Tu posicion', ' ', recicladorMarquer);

        animateCameraToPosicion(_position.latitude, _position.longitude);
        isCloseToRecicladorPosition();
        refresh();
      });
    } catch (e) {
      print("Error $e");
    }
  }

  void checkPGS() async {
    bool isLocationEnabled = await Geolocator.isLocationServiceEnabled();

    if (isLocationEnabled) {
      updateLocation();
    } else {
      bool locationGPS = await location.Location().requestService();
      if (locationGPS) {
        updateLocation();
      }
    }
  }

  Future animateCameraToPosicion(double lat, double long) async {
    GoogleMapController controller = await _mapController.future;
    if (controller == null) {
      controller.animateCamera(CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(lat, long), zoom: 13, bearing: 0)));
    }
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition();
  }

  void call() {
    launch("tel://${entrega.client.phone}");
  }

  void isCloseToRecicladorPosition() {
    _distaceBetween = Geolocator.distanceBetween(_position.latitude,
        _position.longitude, entrega.address.lat, entrega.address.lng);

    print('------distancia ${_distaceBetween}------');
  }

  //Metodo actualizar estado de la orden
  void updateToReciclador() async {
    // if (_distaceBetween <= 200) {
    ReesponseApi responseApi =
        await _entregasProvider.updateToEnCaminoReciclador(entrega);
    if (responseApi.success) {
      Fluttertoast.showToast(
          msg: responseApi.msg, toastLength: Toast.LENGTH_LONG);

      Navigator.pushNamedAndRemoveUntil(
          context, 'delivery/orders/list', (route) => false);
    }
    // } else {
    //   MySnackbar.show(context, 'Debes estar mas cerca del contribuyente');
    // }
  }

  void launchWaze() async {
    var url =
        'waze://?11=${entrega.address.lat.toString()}, ${entrega.address.lng.toString()}';
    var fallbackUrl =
        'https://waze.com/ul?11=${entrega.address.lat.toString()},${entrega.address.lng.toString()}&navigate=yes';
    try {
      bool launched =
          await launch(url, forceSafariVC: false, forceWebView: false);
      if (!launched) {
        await launch(fallbackUrl, forceSafariVC: false, forceWebView: false);
      }
    } catch (e) {
      await launch(fallbackUrl, forceSafariVC: false, forceWebView: false);
    }
  }

  void launchGoogleMaps() async {
    var url =
        'google.navigation:q=${entrega.address.lat.toString()}, ${entrega.address.lng.toString()}';
    var fallbackUrl =
        'https://www.google.com/maps/search/?api=1&query=${entrega.address.lat.toString()}, ${entrega.address.lng.toString()}';
    try {
      bool launched =
          await launch(url, forceSafariVC: false, forceWebView: false);
      if (!launched) {
        await launch(fallbackUrl, forceSafariVC: false, forceWebView: false);
      }
    } catch (e) {
      await launch(fallbackUrl, forceSafariVC: false, forceWebView: false);
    }
  }
}
