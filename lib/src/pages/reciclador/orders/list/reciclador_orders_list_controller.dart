import 'package:flutter/material.dart';
import 'package:flutter_reciclaje/src/models/entregas.dart';
import 'package:flutter_reciclaje/src/models/user.dart';
import 'package:flutter_reciclaje/src/pages/reciclador/orders/detail/reciclador_entregas_detail_page.dart';
import 'package:flutter_reciclaje/src/provider/entregas_provider.dart';
import 'package:flutter_reciclaje/src/utils/shared_pref.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class RecicladorOrdersListController {
  SharedPref _sharedPref = new SharedPref();
  BuildContext context;
  Function refresh;
  User user;

  List<String> categorias = ['RECOGIDAS', 'EN CAMINO', 'ASIGNADAS'];
  EntregasProvider _entregasProvider = new EntregasProvider();
  GlobalKey<ScaffoldState> key = new GlobalKey<ScaffoldState>();
  bool isUpdate;

  Future init(BuildContext context, Function refresh) async {
    this.context = context;
    this.refresh = refresh;
    user = User.fromJson(await _sharedPref.read('user'));
    _entregasProvider.init(context);
    refresh();
  }

  Future<List<Entregas>> getEntregas(String status) async {
    print(status);
    return await _entregasProvider.getByRecicladorStatus(user?.id, status);
  }

  void logout() {
    _sharedPref.logout(context);
  }

  void openDrawer() {
    key.currentState.openDrawer();
  }

  void goToRoles() {
    Navigator.pushNamedAndRemoveUntil(context, 'roles', (route) => false);
  }

  void openBottonShett(Entregas entrega) async {
    await showMaterialModalBottomSheet(
        context: context,
        builder: (context) => RecicladorEntregasDetailPage(entrega: entrega));

    // if (isUpdate) {
    refresh();
    // }
  }

  void goToUpdatePage() {
    Navigator.pushNamed(context, 'client/update');
  }
}
