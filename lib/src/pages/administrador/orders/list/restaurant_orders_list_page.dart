import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_reciclaje/src/models/entregas.dart';
import 'package:flutter_reciclaje/src/pages/administrador/orders/list/restaurant_orders_list_controller.dart';
import 'package:flutter_reciclaje/src/utils/my_colors.dart';
import 'package:flutter_reciclaje/src/utils/relative_time_util.dart';
import 'package:flutter_reciclaje/src/widgets/no_widget_data.dart';

class RestaurantOrdersListPage extends StatefulWidget {
  const RestaurantOrdersListPage({Key key}) : super(key: key);

  @override
  _RestaurantOrdersListPageState createState() =>
      _RestaurantOrdersListPageState();
}

class _RestaurantOrdersListPageState extends State<RestaurantOrdersListPage> {
  RestaurantOrdersListController _con = new RestaurantOrdersListController();

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      _con.init(context, refresh);
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: _con.categorias?.length,
        child: Scaffold(
          key: _con.key,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(100),
            child: AppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Colors.white,
              flexibleSpace: Column(
                children: [
                  SizedBox(height: 40),
                  _menuDrawer(),
                ],
              ),
              // actions: [_comprasBolsa()],
              bottom: TabBar(
                indicatorColor: MyColors.primaryColor,
                labelColor: Colors.black,
                unselectedLabelColor: Colors.grey[400],
                isScrollable: true,
                tabs: List<Widget>.generate(_con.categorias.length, (index) {
                  return Tab(child: Text(_con.categorias[index] ?? ''));
                }),
              ),
            ),
          ),
          drawer: _drawer(),
          body: TabBarView(
              children: _con.categorias.map((String status) {
            //return _cardEntrega(null);
            return FutureBuilder(
              future: _con.getEntregas(status),
              builder: (BuildContext context,
                  AsyncSnapshot<List<Entregas>> snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.length > 0) {
                    return ListView.builder(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                        itemCount: snapshot.data?.length ?? 0,
                        itemBuilder: (_, index) {
                          return _cardEntrega(snapshot.data[index]);
                        });
                  } else {
                    return NoDataWidget(texto: 'No existen entregas');
                  }
                } else {
                  return NoDataWidget(texto: 'No exiten entregas');
                }
              },
            );
          }).toList()),
        ));
  }

  Widget _cardEntrega(Entregas entrega) {
    return GestureDetector(
      onTap: () {
        _con.openBottonShett(entrega);
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        height: 150,
        child: Card(
          elevation: 3.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          child: Stack(
            children: [
              Positioned(
                child: Container(
                    height: 30,
                    width: MediaQuery.of(context).size.width * 0.9,
                    decoration: BoxDecoration(
                        color: MyColors.primaryColor,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                          topRight: Radius.circular(15),
                        )),
                    child: Container(
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: Text('Entrega #${entrega.id}',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontFamily: 'NimbusSans')),
                    )),
              ),
              Container(
                  margin: EdgeInsets.only(top: 40, left: 20),
                  child: Column(children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.symmetric(vertical: 5),
                      width: double.infinity,
                      child: Text(
                        'fecha: ${RelativeTimeUtil.getRelativeTime(entrega?.timestamp ?? 0)}',
                        style: TextStyle(fontSize: 13),
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.symmetric(vertical: 5),
                      width: double.infinity,
                      child: Text(
                        'Contribuyente: ${entrega?.client?.name ?? ''} '
                        '${entrega?.client?.lastname ?? ''}',
                        style: TextStyle(fontSize: 13),
                        maxLines: 1,
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.symmetric(vertical: 5),
                      width: double.infinity,
                      child: Text(
                        'Recoger en: ${entrega?.address?.address ?? ''}',
                        style: TextStyle(fontSize: 13),
                        maxLines: 1,
                      ),
                    ),
                  ]))
            ],
          ),
        ),
      ),
    );
  }

  Widget _menuDrawer() {
    return GestureDetector(
      onTap: _con.openDrawer,
      child: Container(
        margin: EdgeInsets.only(left: 20),
        alignment: Alignment.centerLeft,
        child: Image.asset(
          'assets/img/menu.png',
          width: 20,
          height: 20,
        ),
      ),
    );
  }

  Widget _drawer() {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
              decoration: BoxDecoration(color: MyColors.primaryColor),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${_con.user?.name ?? ' '} ${_con.user?.lastname ?? ''}',
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                      maxLines: 1,
                    ),
                    Text(
                      '${_con.user?.email ?? ' '}',
                      style: TextStyle(
                          fontSize: 13,
                          color: Colors.grey[200],
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic),
                      maxLines: 1,
                    ),
                    Text(
                      '${_con.user?.phone ?? ' '}',
                      style: TextStyle(
                          fontSize: 13,
                          color: Colors.grey[200],
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic),
                      maxLines: 1,
                    ),
                    Container(
                      height: 75,
                      margin: EdgeInsets.only(top: 10),
                      child: FadeInImage(
                        image: _con.user?.image != null
                            ? NetworkImage(_con.user?.image)
                            : AssetImage('assets/img/user_profile_2.png'),
                        fit: BoxFit.contain,
                        fadeInDuration: Duration(milliseconds: 50),
                        placeholder:
                            AssetImage('assets/img/user_profile_2.png'),
                      ),
                    )
                  ])),
          ListTile(
            onTap: _con.goToUpdatePage,
            title: Text('Editar perfil'),
            trailing: Icon(Icons.edit_outlined),
            // leading:  Icon(Icons.edit_outlined),
          ),
          // ListTile(
          //     title: Text('Mis pedidos'),
          //     trailing: Icon(Icons.shopping_cart_outlined)),
          _con.user != null
              ? _con.user.roles.length > 1
                  ? ListTile(
                      onTap: _con.goToRoles,
                      title: Text('Seleccionar rol'),
                      trailing: Icon(Icons.person_outline),
                    )
                  : Container()
              : Container(),
          ListTile(
              onTap: _con.logout,
              title: Text('Cerrar sesion'),
              trailing: Icon(Icons.power_settings_new))
        ],
      ),
    );
  }

  void refresh() {
    setState(() {});
  }
}
