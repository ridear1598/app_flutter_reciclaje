import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_reciclaje/src/models/entregas.dart';
import 'package:flutter_reciclaje/src/models/user.dart';
import 'package:flutter_reciclaje/src/pages/administrador/orders/detail/admin_entregas_detail_controller.dart';
import 'package:flutter_reciclaje/src/utils/my_colors.dart';
import 'package:flutter_reciclaje/src/utils/relative_time_util.dart';

class AdminEntregasDetailPage extends StatefulWidget {
  Entregas entrega;

  AdminEntregasDetailPage({Key key, @required this.entrega}) : super(key: key);

  @override
  _AdminEntregasDetailPageState createState() =>
      _AdminEntregasDetailPageState();
}

class _AdminEntregasDetailPageState extends State<AdminEntregasDetailPage> {
  AdminEntregasControllerDetail _con = new AdminEntregasControllerDetail();
  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      _con.init(context, refresh, widget.entrega);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Contribucion #${_con.entrega?.id ?? ''}'),
        ),
        bottomNavigationBar: Container(
          height: MediaQuery.of(context).size.height * 0.550,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Divider(
                  color: Colors.grey[400],
                  endIndent: 30,
                  indent: 30,
                ),
                _textDescripcion(),
                SizedBox(height: 10),
                _con.entrega.status != 'PENDIENTES'
                    ? _deliveryData()
                    : Container(),
                _con.entrega.status == 'PENDIENTES'
                    ? _dropDown(_con.users)
                    : Container(),
                //TextTotal price,
                _textData(
                    'Contribuyente: ',
                    '${_con.entrega.client?.name ?? ''} '
                        ' ${_con.entrega.client?.lastname ?? ''}'),
                _textData(
                    'Recoger en: ', '${_con.entrega.address?.address ?? ''}'),
                _textData('Fecha: ',
                    '${RelativeTimeUtil.getRelativeTime(_con.entrega?.timestamp ?? 0)} '),
                _con.entrega.status == 'PENDIENTES'
                    ? _buttonNext()
                    : Container()
              ],
            ),
          ),
        ),
        body: ListView(children: [_cardEntrega(_con.entrega)]));
  }

  Widget _textData(String title, String contenido) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: ListTile(
          title: Text(title),
          subtitle: Text(contenido, maxLines: 2),
        ));
  }

  Widget _dropDown(List<User> users) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      child: Material(
        elevation: 2.0,
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(5)),
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: DropdownButton(
                    underline: Container(
                      alignment: Alignment.center,
                    ),
                    elevation: 3,
                    isExpanded: true,
                    hint: Text(
                      'Recicladores',
                      style: TextStyle(color: Colors.grey, fontSize: 16),
                    ),
                    items: _dropDownItems(users),
                    value: _con.idDelivery,
                    onChanged: (option) {
                      setState(() {
                        print('Repartidor seleccionada $option');
                        _con.idDelivery = option;
                      });
                    },
                  ))
            ],
          ),
        ),
      ),
    );
  }

  Widget _deliveryData() {
    return Row(children: [
      Container(
        height: 40,
        width: 40,
      ),
      Text(
          '${_con.entrega.reciclador?.name ?? ''} ${_con.entrega.reciclador?.lastname ?? ''}')
    ]);
  }

  List<DropdownMenuItem<String>> _dropDownItems(List<User> user) {
    List<DropdownMenuItem<String>> list = [];
    user.forEach((user) {
      list.add(DropdownMenuItem(
        child: Text(user.name),
        value: user.id,
      ));
    });
    return list;
  }

  Widget _textDescripcion() {
    return Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.symmetric(horizontal: 30),
      child: Text(
          _con.entrega?.status == 'PENDIENTES'
              ? 'Asignar reciclador'
              : 'Reciclador Asignado',
          style: TextStyle(
              fontStyle: FontStyle.italic, color: Colors.red, fontSize: 16)),
    );
  }

  Widget _buttonNext() {
    return Container(
      margin: EdgeInsets.only(left: 30, right: 30, top: 5, bottom: 30),
      child: ElevatedButton(
        onPressed: _con.updateEntrega,
        style: ElevatedButton.styleFrom(
          primary: MyColors.primaryColor,
          padding: EdgeInsets.symmetric(vertical: 5),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: Container(
                height: 45,
                alignment: Alignment.center,
                child: Text(
                  'Recoger contribucion',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: EdgeInsets.only(left: 50, top: 4),
                height: 30,
                child: Icon(Icons.check_circle, color: Colors.white, size: 30),
              ),
            )
          ],
        ),
      ),
    );
  }

  // Widget _cardEntrega(Entregas entrega) {
  //   return Container(
  //     margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
  //     child: Row(
  //       children: [
  //         Column(
  //           children: [
  //             Text(entrega?.descripcion ?? '',
  //                 style: TextStyle(fontWeight: FontWeight.bold)),
  //             SizedBox(height: 10),
  //             Text('Peso aproximado :${entrega?.cantidad ?? '0'} Kilogramos',
  //                 style: TextStyle(fontWeight: FontWeight.bold)),
  //           ],
  //         )
  //       ],
  //     ),
  //   );
  // }

  Widget _cardEntrega(Entregas entrega) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        height: 150,
        child: Card(
          elevation: 3.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          child: Stack(
            children: [
              Positioned(
                child: Container(
                    height: 30,
                    width: MediaQuery.of(context).size.width * 0.9,
                    decoration: BoxDecoration(
                        color: MyColors.primaryColor,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                          topRight: Radius.circular(15),
                        )),
                    child: Container(
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: Text('Detalles',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontFamily: 'NimbusSans')),
                    )),
              ),
              Container(
                  margin: EdgeInsets.only(top: 40, left: 20),
                  child: Column(children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.symmetric(vertical: 5),
                      width: double.infinity,
                      child: Text(
                        'Tipo: ${entrega?.tipo ?? 'No asignado'}',
                        style: TextStyle(fontSize: 13),
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.symmetric(vertical: 5),
                      width: double.infinity,
                      child: Text(
                        'Descripcion: ${entrega?.descripcion ?? 'No asignado'} ',
                        style: TextStyle(fontSize: 13),
                        maxLines: 2,
                      ),
                    ),
                  ]))
            ],
          ),
        ),
      ),
    );
  }

  void refresh() {
    setState(() {});
  }
}
