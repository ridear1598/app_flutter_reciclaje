import 'package:flutter/cupertino.dart';
import 'package:flutter_reciclaje/src/models/entregas.dart';
import 'package:flutter_reciclaje/src/models/response_api.dart';
import 'package:flutter_reciclaje/src/models/user.dart';
import 'package:flutter_reciclaje/src/provider/entregas_provider.dart';
import 'package:flutter_reciclaje/src/provider/users_provider.dart';
import 'package:flutter_reciclaje/src/utils/shared_pref.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AdminEntregasControllerDetail {
  SharedPref _sharedPref = new SharedPref();
  BuildContext context;
  Function refresh;
  User user;
  Entregas entrega;
  double total = 0;
  List<User> users = [];
  String idDelivery;
  UsersProvider _usersProvider = new UsersProvider();
  EntregasProvider _entregasProvider = new EntregasProvider();

  Future init(BuildContext context, Function refresh, Entregas entrega) async {
    this.context = context;
    this.refresh = refresh;
    this.entrega = entrega;
    user = User.fromJson(await _sharedPref.read('user'));
    _usersProvider.init(context);
    _entregasProvider.init(context);
    getTotal();
    getUsers();
    refresh();
  }

  void getUsers() async {
    users = await _usersProvider.getReciladorMen();
    refresh();
  }

  void getTotal() {
    total = 0;
    refresh();
  }

  void updateEntrega() async {
    if (idDelivery != null) {
      entrega.idDelivery = idDelivery;
      print('Lista de entregas ${entrega.idAdress}');
      ReesponseApi responseApi =
          await _entregasProvider.updateToDispach(entrega);
      Fluttertoast.showToast(
          msg: responseApi.msg, toastLength: Toast.LENGTH_LONG);
      Navigator.pop(context, true);
    } else {
      Fluttertoast.showToast(msg: 'Seleccionar repartidor');
    }
  }
}
