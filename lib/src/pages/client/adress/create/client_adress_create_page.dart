import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_reciclaje/src/pages/client/adress/create/client_adress_create_controller.dart';
import 'package:flutter_reciclaje/src/utils/my_colors.dart';

class ClientAdressCreatePage extends StatefulWidget {
  const ClientAdressCreatePage({Key key}) : super(key: key);

  @override
  _ClientAdressCreatePageState createState() => _ClientAdressCreatePageState();
}

class _ClientAdressCreatePageState extends State<ClientAdressCreatePage> {
  ClientAdreesCreateController _con = new ClientAdreesCreateController();
  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      _con..init(context, refresh);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Crear direccion"),
      ),
      body: Column(
        children: [
          _textCompleteData(),
          _textFieldAdress(),
          _textFieldBarrio(),
          _textFieldReferencia()
        ],
      ),
      bottomNavigationBar: _buttonAccept(),
    );
  }

  Widget _textFieldAdress() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: TextField(
        controller: _con.adressController,
        decoration: InputDecoration(
            labelText: 'Direccion',
            suffixIcon: Icon(Icons.location_on, color: MyColors.primaryColor)),
      ),
    );
  }

  Widget _textFieldReferencia() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: TextField(
        controller: _con.refPointController,
        onTap: _con.openMap,
        autofocus: false,
        focusNode: AlwaysDisableFocusNode(),
        decoration: InputDecoration(
            labelText: 'Referencia',
            suffixIcon: Icon(Icons.map, color: MyColors.primaryColor)),
      ),
    );
  }

  Widget _textFieldBarrio() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: TextField(
        controller: _con.barrioController,
        decoration: InputDecoration(
            labelText: 'Barrio',
            suffixIcon:
                Icon(Icons.location_city, color: MyColors.primaryColor)),
      ),
    );
  }

  Widget _textCompleteData() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 40, vertical: 30),
      child: Text("Completa estos datos",
          style: TextStyle(
            fontSize: 19,
            fontWeight: FontWeight.bold,
          )),
    );
  }

  Widget _buttonAccept() {
    return Container(
      height: 50,
      margin: EdgeInsets.symmetric(vertical: 30, horizontal: 50),
      width: double.infinity,
      child: ElevatedButton(
        onPressed: _con.crearDireccion,
        child: Text('Aceptar'),
        style: ElevatedButton.styleFrom(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            primary: MyColors.primaryColor),
      ),
    );
  }

  void refresh() {
    setState(() {});
  }
}

class AlwaysDisableFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}
