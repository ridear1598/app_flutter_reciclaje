import 'package:flutter/material.dart';
import 'package:flutter_reciclaje/src/models/address.dart';
import 'package:flutter_reciclaje/src/models/response_api.dart';
import 'package:flutter_reciclaje/src/models/user.dart';
import 'package:flutter_reciclaje/src/pages/client/adress/map/client_adress_map_page.dart';
import 'package:flutter_reciclaje/src/provider/address_provider.dart';
import 'package:flutter_reciclaje/src/utils/my-snackbar.dart';
import 'package:flutter_reciclaje/src/utils/shared_pref.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class ClientAdreesCreateController {
  BuildContext context;
  Function refresh;

  TextEditingController refPointController = new TextEditingController();
  TextEditingController adressController = new TextEditingController();
  TextEditingController barrioController = new TextEditingController();

  Map<String, dynamic> refPoint;
  AddressProvider _addressProvider = new AddressProvider();
  User user;

  SharedPref __sharedPreferences = new SharedPref();

  Future init(BuildContext context, Function refresh) async {
    this.context = context;
    this.refresh = refresh;
    user = User.fromJson(await __sharedPreferences.read('user'));

    _addressProvider.init(context);
  }

  void openMap() async {
    //   showModalBottomSheet(
    //       context: context, builder: (context) => ClientAdressMapPage());
    // }
    refPoint = await showMaterialModalBottomSheet(
        context: context,
        isDismissible: false,
        enableDrag: false,
        builder: (context) => ClientAdressMapPage());

    if (refPoint != null) {
      refPointController.text = refPoint['adress'];
      refresh();
    }
  }

  void crearDireccion() async {
    String direccion = adressController.text;
    String barrio = barrioController.text;
    double lat = refPoint['lat'] ?? 0;
    double lng = refPoint['lng'] ?? 0;

    if (direccion.isEmpty || barrio.isEmpty || lat == 0 || lng == 0) {
      MySnackbar.show(context, 'completa todos los campos');
      return;
    }
    Address adress = new Address(
        address: direccion,
        neighbordhood: barrio,
        lat: lat,
        lng: lng,
        idUser: user.id);
    ReesponseApi responseApi = await _addressProvider.create(adress);

    if (responseApi.success) {
      adress.id = responseApi.data;
      __sharedPreferences.save('address', adress);

      Fluttertoast.showToast(msg: responseApi.msg);
      Navigator.pop(context, true);
    }
  }
}
