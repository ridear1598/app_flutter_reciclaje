import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_reciclaje/src/models/address.dart';
import 'package:flutter_reciclaje/src/pages/client/adress/list/client_adress_list_controller.dart';
import 'package:flutter_reciclaje/src/utils/my_colors.dart';

class ClientAdressListPage extends StatefulWidget {
  const ClientAdressListPage({Key key}) : super(key: key);

  @override
  _ClientAdressListPageState createState() => _ClientAdressListPageState();
}

class _ClientAdressListPageState extends State<ClientAdressListPage> {
  ClientAdreesListController _con = new ClientAdreesListController();
  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      _con..init(context, refresh);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Direcciones"),
        actions: [_iconAdd()],
      ),
      body: Stack(
        children: [
          Positioned(child: _textSelectAdress()),
          Container(
              margin: EdgeInsets.only(top: 50), child: _listaDirecciones())
        ],
      ),
      bottomNavigationBar: _buttonAccept(),
    );
  }

  Widget _noAddressed() {
    return Column(
      children: [
        Container(
          width: double.infinity,
          child: Column(
            children: [
              _textSelectAdress(),
              Container(
                  margin: EdgeInsets.only(top: 40), child: _buttonNewAdress()),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buttonNewAdress() {
    return Container(
      height: 40,
      child: ElevatedButton(
        onPressed: _con.goToNewAdrees,
        child: Text('Nueva direccion'),
        style: ElevatedButton.styleFrom(primary: Colors.green),
      ),
    );
  }

  Widget _buttonAccept() {
    return Container(
      height: 50,
      margin: EdgeInsets.symmetric(vertical: 30, horizontal: 50),
      width: double.infinity,
      child: ElevatedButton(
        onPressed: _con.irAtras,
        child: Text('Aceptar'),
        style: ElevatedButton.styleFrom(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            primary: MyColors.primaryColor),
      ),
    );
  }

  Widget _listaDirecciones() {
    return FutureBuilder(
        future: _con.getAdrees(),
        builder: (context, AsyncSnapshot<List<Address>> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.length > 0) {
              return ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                itemCount: snapshot.data?.length ?? 0,
                itemBuilder: (_, index) {
                  return _radioSelectorDireccion(snapshot.data[index], index);
                },
              );
            } else {
              return _noAddressed();
            }
          } else {
            return _noAddressed();
          }
        });
  }

  Widget _radioSelectorDireccion(Address address, int index) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          Row(
            children: [
              Radio(
                  value: index,
                  groupValue: _con.radioValue,
                  onChanged: _con.selecionarRadioValueChange),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    address?.address ?? '',
                    style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    address?.neighbordhood ?? '',
                    style: TextStyle(fontSize: 12),
                  ),
                ],
              ),
            ],
          ),
          Divider(
            color: Colors.grey[400],
          )
        ],
      ),
    );
  }

  Widget _textSelectAdress() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 40, vertical: 30),
      child: Text("Elige tu direccion",
          style: TextStyle(
            fontSize: 19,
            fontWeight: FontWeight.bold,
          )),
    );
  }

  Widget _iconAdd() {
    return IconButton(
        onPressed: _con.goToNewAdrees,
        icon: Icon(Icons.add, color: Colors.white));
  }

  void refresh() {
    setState(() {});
  }
}
