import 'package:flutter/material.dart';
import 'package:flutter_reciclaje/src/models/address.dart';
import 'package:flutter_reciclaje/src/models/user.dart';
import 'package:flutter_reciclaje/src/provider/address_provider.dart';
import 'package:flutter_reciclaje/src/utils/shared_pref.dart';

class ClientAdreesListController {
  BuildContext context;
  Function refresh;
  List<Address> address = [];
  AddressProvider _addressProvider = new AddressProvider();
  User user;
  SharedPref _sharedPref = new SharedPref();
  int radioValue = 0;
  bool iscreated;

  Future init(BuildContext context, Function refresh) async {
    this.context = context;
    this.refresh = refresh;
    user = User.fromJson(await _sharedPref.read('user'));

    _addressProvider.init(context);
    refresh();
  }

  Future<List<Address>> getAdrees() async {
    address = await _addressProvider.getByUser(user.id);
    Address a = Address.fromJson(await _sharedPref.read('address') ?? {});
    int index = address.indexWhere((ad) => ad.id == a.id);
    if (index != -1) {
      radioValue = index;
    }
    print('Se guardo la direccion ${a.toJson()}');
    return address;
  }

  void goToNewAdrees() async {
    var result = await Navigator.pushNamed(context, 'client/adress/create');
    if (result != null) {
      if (result) {
        refresh();
      }
    }
  }

  void selecionarRadioValueChange(int value) async {
    radioValue = value;
    _sharedPref.save('address', address[value]);

    refresh();
    print('Valor seleccionado: $radioValue');
  }

  void irAtras() {
    // Navigator.pushReplacementNamed(context, 'login');
    Navigator.pushNamed(context, 'client/products/list');
  }
}
