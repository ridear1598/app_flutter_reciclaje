import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_reciclaje/src/pages/client/adress/map/client_adress_map_controller.dart';
import 'package:flutter_reciclaje/src/utils/my_colors.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ClientAdressMapPage extends StatefulWidget {
  const ClientAdressMapPage({Key key}) : super(key: key);

  @override
  _ClientAdressMapPageState createState() => _ClientAdressMapPageState();
}

class _ClientAdressMapPageState extends State<ClientAdressMapPage> {
  ClientAdreesMapController _con = new ClientAdreesMapController();
  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      _con..init(context, refresh);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ubica tu direccion en el mapa"),
      ),
      body: Stack(
        children: [
          _googleMaps(),
          Container(
            alignment: Alignment.center,
            child: iconMyLocation(),
          ),
          Container(
            alignment: Alignment.topCenter,
            margin: EdgeInsets.only(top: 30),
            child: _cardAdress(),
          ),
          Container(alignment: Alignment.bottomCenter, child: _buttonAccept())
        ],
      ),
    );
  }

  Widget iconMyLocation() {
    return Image.asset('assets/img/my_location.png', width: 65, height: 65);
  }

  Widget _googleMaps() {
    return GoogleMap(
      mapType: MapType.normal,
      initialCameraPosition: _con.initialPosition,
      onMapCreated: _con.onMapCreated,
      myLocationButtonEnabled: true,
      myLocationEnabled: true,
      onCameraMove: (posicion) {
        _con.initialPosition = posicion;
      },
      onCameraIdle: () async {
        await _con.setLocationDraggableInfo();
      },
    );
  }

  Widget _cardAdress() {
    return Container(
      child: Card(
        color: Colors.grey[800],
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Text(
            _con.adreesName ?? '',
            style: TextStyle(
                color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }

  Widget _buttonAccept() {
    return Container(
      height: 50,
      margin: EdgeInsets.symmetric(vertical: 30, horizontal: 70),
      width: double.infinity,
      child: ElevatedButton(
        onPressed: _con.selectRefPoint,
        child: Text('Seleccionar este punto'),
        style: ElevatedButton.styleFrom(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            primary: MyColors.primaryColor),
      ),
    );
  }

  void refresh() {
    setState(() {});
  }
}
