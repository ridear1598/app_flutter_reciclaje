import 'package:flutter/material.dart';
import 'package:flutter_reciclaje/src/models/response_api.dart';
import 'package:flutter_reciclaje/src/models/user.dart';
import 'package:flutter_reciclaje/src/provider/users_provider.dart';
import 'package:flutter_reciclaje/src/utils/my-snackbar.dart';
import 'package:flutter_reciclaje/src/utils/shared_pref.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ClientUpdateController {
  BuildContext context;
  TextEditingController nameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();

  UsersProvider usersProvider = new UsersProvider();
  User user;
  Function refresh;
  SharedPref _sharedPref = new SharedPref();

  //Inicializar las variables
  Future init(BuildContext context, Function refresh) async {
    this.context = context;
    await usersProvider.init(context);
    user = User.fromJson(await _sharedPref.read('user'));
    print('users son ${user.lastname}');
    nameController.text = user.name;
    lastNameController.text = user.lastname;
    phoneController.text = user.phone;
    refresh();
  }

  void update() async {
    String nombre = nameController.text;
    String apellidos = lastNameController.text;
    String telefono = phoneController.text.trim();

    if (nombre.isEmpty || apellidos.isEmpty || telefono.isEmpty) {
      MySnackbar.show(context, 'Ingresar todos los campos');
      return;
    }

    User myUser = new User(
        name: nombre, phone: telefono, lastname: apellidos, id: user.id);

    ReesponseApi responseApi = await usersProvider.update(myUser);
    //MySnackbar.show(context, responseApi.msg);
    Fluttertoast.showToast(msg: responseApi.msg);

    if (responseApi.success) {
      user = await usersProvider.getById(myUser.id); //Obtenemos el usuario
      _sharedPref.save('user', user.toJson());
      Navigator.pushNamedAndRemoveUntil(
          context, 'client/products/list', (route) => false);
    }
  }
}
