import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_reciclaje/src/pages/client/update/client_update_controller.dart';
import 'package:flutter_reciclaje/src/utils/my_colors.dart';

class ClienteUpdatePage extends StatefulWidget {
  const ClienteUpdatePage({Key key}) : super(key: key);

  @override
  _ClientUpdatePageState createState() => _ClientUpdatePageState();
}

class _ClientUpdatePageState extends State<ClienteUpdatePage> {
  ClientUpdateController _con = new ClientUpdateController();

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      _con.init(context, refresh);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Editar perfil')),
      body: Container(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 50),
              _imageUser(),
              SizedBox(height: 30),
              _textFielName(),
              _textFielApellido(),
              _textFielPhone(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: _buttonRegister(),
    );
  }

  Widget _imageUser() {
    return Container(
      // width: double.infinity,
      // margin: EdgeInsets.only(top: 100),
      child: CircleAvatar(
        backgroundImage: AssetImage('assets/img/user_profile_2.png'),
        radius: 80,
        backgroundColor: Colors.grey[200],
      ),
    );
  }

  Widget _textFielName() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.primaryOpacityColor,
            borderRadius: BorderRadius.circular(30)),
        child: TextField(
          controller: _con.nameController,
          decoration: InputDecoration(
              hintText: 'Nombre',
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(15),
              hintStyle: TextStyle(color: MyColors.primaryColorDark),
              prefixIcon: Icon(Icons.person, color: MyColors.primaryColor)),
        ));
  }

  Widget _textFielApellido() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.primaryOpacityColor,
            borderRadius: BorderRadius.circular(30)),
        child: TextField(
          controller: _con.lastNameController,
          decoration: InputDecoration(
              hintText: 'Apellido',
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(15),
              hintStyle: TextStyle(color: MyColors.primaryColorDark),
              prefixIcon:
                  Icon(Icons.person_outline, color: MyColors.primaryColor)),
        ));
  }

  Widget _textFielPhone() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
        decoration: BoxDecoration(
            color: MyColors.primaryOpacityColor,
            borderRadius: BorderRadius.circular(30)),
        child: TextField(
          keyboardType: TextInputType.phone,
          controller: _con.phoneController,
          decoration: InputDecoration(
              hintText: 'Telefono',
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(15),
              hintStyle: TextStyle(color: MyColors.primaryColorDark),
              prefixIcon: Icon(Icons.phone, color: MyColors.primaryColor)),
        ));
  }

  Widget _buttonRegister() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 50, vertical: 30),
      width: double.infinity, //Abarca toda la pantalla
      child: ElevatedButton(
          onPressed: _con.update,
          child: Text('Actualizar perfil'),
          style: ElevatedButton.styleFrom(
              primary: MyColors.primaryColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),
              padding: EdgeInsets.symmetric(vertical: 15))),
    );
  }

  void refresh() {
    setState(() {});
  }
}
