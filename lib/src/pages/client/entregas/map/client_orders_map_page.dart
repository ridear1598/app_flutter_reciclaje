import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_reciclaje/src/pages/reciclador/orders/map/entregas_orders_map_controller.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ClientOrdenesMapPage extends StatefulWidget {
  const ClientOrdenesMapPage({Key key}) : super(key: key);

  @override
  _ClientOrdenesMapPageState createState() => _ClientOrdenesMapPageState();
}

class _ClientOrdenesMapPageState extends State<ClientOrdenesMapPage> {
  EntregasOrdenesMapController _con = new EntregasOrdenesMapController();
  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      _con..init(context, refresh);
    });
  }

  @override
  void dispose() {
    super.dispose();
    _con.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
              height: MediaQuery.of(context).size.height * 0.65,
              child: _googleMaps()),
          SafeArea(
            child: Column(
              children: [_buttonCenterPosition(), Spacer(), _cardOrderInfo()],
            ),
          ),
        ],
      ),
    );
  }

  Widget _googleMaps() {
    return GoogleMap(
      mapType: MapType.normal,
      initialCameraPosition: _con.initialPosition,
      onMapCreated: _con.onMapCreated,
      myLocationButtonEnabled: true,
      myLocationEnabled: true,
      markers: Set<Marker>.of(_con.markers.values),
      polylines: _con.polylines,
    );
  }

  Widget _listTitleAdress(String title, String subtitle, IconData icon) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 30),
        child: ListTile(
          title: Text(
            title,
            style: TextStyle(fontSize: 13),
          ),
          subtitle: Text(subtitle),
          trailing: Icon(icon),
        ));
  }

  Widget _cardOrderInfo() {
    return Container(
      height: MediaQuery.of(context).size.height * 0.35,
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30), topLeft: Radius.circular(30)),
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3))
          ]),
      child: Column(
        children: [
          _listTitleAdress(_con.entrega?.address?.neighbordhood ?? '', 'Barrio',
              Icons.my_location),
          _listTitleAdress(_con.entrega?.address?.address ?? '', 'Direccion',
              Icons.location_on),
          Divider(
            color: Colors.grey[400],
            endIndent: 30,
            indent: 30,
          ),
          clienteInfo(),
        ],
      ),
    );
  }

  Widget clienteInfo() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 35, vertical: 20),
      child: Row(
        children: [
          Container(
            height: 50,
            width: 50,
            child: FadeInImage(
              image: _con.entrega?.reciclador?.image != null
                  ? NetworkImage(_con.entrega?.reciclador?.image)
                  : AssetImage('assets/img/no-image.png'),
              fit: BoxFit.contain,
              fadeInDuration: Duration(milliseconds: 50),
              placeholder: AssetImage('assets/img/no-image.png'),
            ),
          ),
          Container(
              margin: EdgeInsets.only(left: 10),
              child: Text(
                '${_con.entrega?.reciclador?.name ?? ''}  ${_con.entrega?.reciclador?.lastname ?? ''}',
                style: TextStyle(color: Colors.black, fontSize: 17),
                maxLines: 1,
              )),
          Spacer(),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.grey[200],
            ),
            child: IconButton(
                onPressed: _con.call,
                icon: Icon(Icons.phone, color: Colors.black)),
          )
        ],
      ),
    );
  }

  Widget _buttonCenterPosition() {
    return GestureDetector(
      onTap: () {},
      child: Container(
        alignment: Alignment.centerRight,
        margin: EdgeInsets.symmetric(horizontal: 5),
        child: Card(
          shape: CircleBorder(),
          color: Colors.white,
          elevation: 4.0,
          child: Container(
            padding: EdgeInsets.all(10),
            child: Icon(
              Icons.location_searching,
              color: Colors.grey[600],
              size: 20,
            ),
          ),
        ),
      ),
    );
  }

  Widget _iconWze() {
    return GestureDetector(
        onTap: _con.launchWaze,
        child: Image.asset(
          'assets/img/waze.png',
          height: 30,
          width: 40,
        ));
  }

  Widget _iconGoogleMaps() {
    return GestureDetector(
        onTap: _con.launchGoogleMaps,
        child: Image.asset(
          'assets/img/google_maps.png',
          height: 30,
          width: 40,
        ));
  }

  void refresh() {
    if (!mounted) return;
    setState(() {});
  }
}
