import 'package:flutter/material.dart';
import 'package:flutter_reciclaje/src/models/address.dart';
import 'package:flutter_reciclaje/src/models/entregas.dart';
import 'package:flutter_reciclaje/src/models/response_api.dart';
import 'package:flutter_reciclaje/src/models/user.dart';
import 'package:flutter_reciclaje/src/provider/entregas_provider.dart';
import 'package:flutter_reciclaje/src/utils/shared_pref.dart';

class Entregascontroller {
  BuildContext context;
  Function refresh;
  List<Address> address = [];
  EntregasProvider _entregasProvider = new EntregasProvider();
  User user;
  SharedPref _sharedPref = new SharedPref();
  int radioValue = 0;
  bool iscreated;
  TextEditingController descriptionController = new TextEditingController();
  TextEditingController cantidadController = new TextEditingController();
  TextEditingController tipoController = new TextEditingController();

  Future init(BuildContext context, Function refresh) async {
    this.context = context;
    this.refresh = refresh;
    user = User.fromJson(await _sharedPref.read('user'));

    _entregasProvider.init(context);
    refresh();
  }

  void saveEntrega() async {
    String cantidad = cantidadController.text;
    String descripcion = descriptionController.text;
    String tipo = tipoController.text;
    Address a = Address.fromJson(await _sharedPref.read('address') ?? {});

    print('tipoassa' + tipo);

    Entregas entregas = new Entregas(
        idClient: user.id,
        idAdress: a.id,
        status: 'PENDIENTES',
        descripcion: descripcion,
        cantidad: int.parse(cantidad),
        tipo: tipo);
    ReesponseApi responseApi = await _entregasProvider.createEntrega(entregas);
    Navigator.pushNamedAndRemoveUntil(
        context, 'client/entregas/list', (route) => false);
    refresh();
    if (responseApi.success) {
      print('mensajes ${responseApi.msg}');
    }
  }
}
