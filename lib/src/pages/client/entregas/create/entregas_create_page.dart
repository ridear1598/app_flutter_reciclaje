import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_reciclaje/src/pages/client/entregas/create/entregas_create_controller.dart';
import 'package:flutter_reciclaje/src/utils/my_colors.dart';

class EntregasPage extends StatefulWidget {
  const EntregasPage({Key key}) : super(key: key);

  @override
  _EntregasPageState createState() => _EntregasPageState();
}

class _EntregasPageState extends State<EntregasPage> {
  Entregascontroller _con = new Entregascontroller();

  @override
  void initState() {
    super.initState();
    _con.init(context, refresh);

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      _con.init(context, refresh);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Crear aporte")),
      body: Column(
        children: [
          _textCompleteData(),
          _textFieldDescription(),
          _textFieldcantidad(),
          _textFieldTipoReciclaje()
        ],
      ),
      bottomNavigationBar: _buttonAccept(),
    );
  }

  Widget _textFieldDescription() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: TextField(
        controller: _con.descriptionController,
        decoration: InputDecoration(
            labelText: 'Descripcion',
            suffixIcon: Icon(Icons.description, color: MyColors.primaryColor)),
      ),
    );
  }

  Widget _textFieldTipoReciclaje() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: TextField(
        controller: _con.tipoController,
        decoration: InputDecoration(
            labelText: 'Tipo',
            suffixIcon:
                Icon(Icons.remove_circle_sharp, color: MyColors.primaryColor)),
      ),
    );
  }

  Widget _textFieldcantidad() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: TextField(
        controller: _con.cantidadController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            labelText: 'Peso aproximado',
            suffixIcon: Icon(Icons.production_quantity_limits,
                color: MyColors.primaryColor)),
      ),
    );
  }

  Widget _textCompleteData() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 40, vertical: 30),
      child: Text("Completa estos datos",
          style: TextStyle(
            fontSize: 19,
            fontWeight: FontWeight.bold,
          )),
    );
  }

  Widget _buttonAccept() {
    return Container(
      height: 50,
      margin: EdgeInsets.symmetric(vertical: 30, horizontal: 50),
      width: double.infinity,
      child: ElevatedButton(
        onPressed: _con.saveEntrega,
        child: Text('Crear'),
        style: ElevatedButton.styleFrom(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            primary: MyColors.primaryColor),
      ),
    );
  }

  void refresh() {
    setState(() {});
  }
}
