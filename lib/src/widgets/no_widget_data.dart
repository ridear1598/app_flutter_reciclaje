import 'package:flutter/material.dart';

class NoDataWidget extends StatelessWidget {
  String texto;

  NoDataWidget({Key key, this.texto}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [Image.asset('assets/img/no_items.png'), Text(texto)],
      ),
    );
  }
}
