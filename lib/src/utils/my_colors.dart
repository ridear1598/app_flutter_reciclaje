import 'package:flutter/material.dart';

class MyColors {
  static Color primaryColor = Color(0xFF25BF51);
  static Color primaryOpacityColor = Color(0xFFE8E8E8);
  static Color primaryColorDark = Color(0xFF1DAA11);
}
