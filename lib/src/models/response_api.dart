import 'dart:convert';

ReesponseApi reesponseApiFromJson(String str) =>
    ReesponseApi.fromJson(json.decode(str));

String reesponseApiToJson(ReesponseApi data) => json.encode(data.toJson());

class ReesponseApi {
  String msg;
  String err;
  bool success;
  dynamic data;

  ReesponseApi({
    this.msg,
    this.err,
    this.success,
  });

  ReesponseApi.fromJson(Map<String, dynamic> json) {
    msg = json["msg"];
    err = json["err"];
    success = json["success"];
    try {
      data = json["data"];
    } catch (e) {
      print('Exception data $e');
    }
  }

  Map<String, dynamic> toJson() =>
      {"msg": msg, "err": err, "success": success, "data": data};
}
