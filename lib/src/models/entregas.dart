import 'dart:convert';
import 'package:flutter_reciclaje/src/models/address.dart';
import 'package:flutter_reciclaje/src/models/user.dart';

Entregas entregasFromJson(String str) => Entregas.fromJson(json.decode(str));

String entregasToJson(Entregas data) => json.encode(data.toJson());

class Entregas {
  Entregas(
      {this.id,
      this.idClient,
      this.idAdress,
      this.idDelivery,
      this.status,
      this.cantidad,
      this.tipo,
      this.descripcion,
      this.timestamp,
      this.lat,
      this.lng,
      this.client,
      this.reciclador,
      this.address});

  String id;
  String idClient;
  String idAdress;
  String idDelivery;
  String status;
  int cantidad;
  String tipo;
  String descripcion;
  int timestamp;
  double lat;
  double lng;
  List<Entregas> toList = [];
  User client;
  User reciclador;
  Address address;

  factory Entregas.fromJson(Map<String, dynamic> json) => Entregas(
      id: json["id"] is int ? json['id'].toString() : json["id"],
      idClient: json["id_client"],
      idAdress: json["id_adress"],
      idDelivery: json["id_delivery"],
      status: json["status"],
      cantidad: json["cantidad"],
      tipo: json["tipo"],
      descripcion: json["descripcion"],
      timestamp: json["timestamp"] is String
          ? int.parse(json["timestamp"])
          : json["timestamp"],
      lat: json["lat"] is String ? double.parse(json["lat"]) : json["lat"],
      lng: json["lng"] is String ? double.parse(json["lng"]) : json["lng"],
      client: json["client"] is String
          ? userFromJson(json["client"])
          : json["client"] is User
              ? json["client"]
              : User.fromJson(json["client"] ?? {}),
      reciclador: json["reciclador"] is String
          ? userFromJson(json["reciclador"])
          : json["reciclador"] is User
              ? json["reciclador"]
              : User.fromJson(json["reciclador"] ?? {}),
      address: json["address"] is String
          ? addressFromJson(json["address"])
          : json["address"] is Address
              ? json["address"]
              : Address.fromJson(json["address"] ?? {}));

  Entregas.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;
    jsonList.forEach((item) {
      Entregas entrega = Entregas.fromJson(item);
      toList.add(entrega);
    });
  }
  Map<String, dynamic> toJson() => {
        "id": id,
        "id_client": idClient,
        "id_adress": idAdress,
        "id_delivery": idDelivery,
        "status": status,
        "cantidad": cantidad,
        "tipo": tipo,
        "descripcion": descripcion,
        "timestamp": timestamp,
        "lat": lat,
        "lng": lng,
        "client": client,
        "reciclador": reciclador,
        "address": address,
      };
}
