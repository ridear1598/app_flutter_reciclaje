import 'dart:convert';

import 'package:flutter_reciclaje/src/models/rol.dart';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  String id;
  String name;
  String dni;
  String lastname;
  String phone;
  String email;
  String password;
  String sessionToken;
  String image;
  List<Rol> roles = [];
  List<User> toList = [];

//Constructor
  User(
      {this.id,
      this.name,
      this.dni,
      this.lastname,
      this.phone,
      this.email,
      this.password,
      this.sessionToken,
      this.image,
      this.roles});

//Resive un json y retorna un objeto de tipo user
  factory User.fromJson(Map<String, dynamic> json) => User(
      id: json["id"] is int ? json['id'].toString() : json["id"],
      name: json["name"],
      dni: json["dni"],
      lastname: json["lastname"],
      phone: json["phone"],
      email: json["email"],
      password: json["password"],
      sessionToken: json["session_token"],
      image: json["image"],
      roles: json["roles"] == null
          ? []
          : List<Rol>.from(json['roles'].map((model) => Rol.fromJson(model))) ??
              []);

  User.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;
    jsonList.forEach((item) {
      User user = User.fromJson(item);
      toList.add(user);
    });
  }
//Toma el objeto user y lo transforma a un json
  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "dni": dni,
        "lastname": lastname,
        "phone": phone,
        "email": email,
        "password": password,
        "session_token": sessionToken,
        "image": image,
        "roles": roles
      };
}
